# Resources related to IAP

# Enabling Identity Aware Proxy API
resource "google_project_service" "iap_api" {
  count   = var.env == "prd" ? 0 : 1
  project = data.google_project.tangelov_project.project_id
  service = "iap.googleapis.com"

  disable_dependent_services = true
}

# Configuring GAE workload for IAP
resource "google_iap_brand" "iap_brand" {
  count             = var.env == "prd" ? 0 : 1
  support_email     = var.iap_support_email
  application_title = "Tangelov ${upper(var.env)} env"
  project           = data.google_project.tangelov_project.project_id
}

# Creation of one client to connect throughout IAP
# The final step is to configure the OAuth consent screen using your web console
resource "google_iap_client" "project_client" {
  count        = var.env == "prd" ? 0 : 1
  display_name = "Development Client"
  brand        = google_iap_brand.iap_brand[count.index].name
}

# Permissions to access to GAE using IAP
resource "google_iap_web_type_app_engine_iam_policy" "policy" {
  count       = var.env == "prd" ? 0 : 1
  project     = google_app_engine_application.app_engine[count.index].project
  app_id      = google_app_engine_application.app_engine[count.index].app_id
  policy_data = data.google_iam_policy.iap_access.policy_data
}

# IAM Policy required to access to resources through Google IAP
data "google_iam_policy" "iap_access" {
  binding {
    role    = "roles/iap.httpsResourceAccessor"
    members = var.iap_enabled_identities
  }
}
