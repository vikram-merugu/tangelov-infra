# Resources related to Cloud Functions
# Enabling Cloud Functions API
resource "google_project_service" "cloud_functions_api" {
  project = data.google_project.tangelov_project.project_id
  service = "cloudfunctions.googleapis.com"

  disable_dependent_services = true
}

# Resources related to messages-to-matrix Cloud Function
# This function will parse messages from a PubSub queue and will
# send it content to a Matrix channel using a bot

# Zipping Messages to matrix code to upload it to GCS
data "archive_file" "gcp_function_messages_matrix_code" {
  type        = "zip"
  source_dir  = "${path.module}/code/messages-to-matrix"
  output_path = "${path.module}/code/dest/${var.cfunction_messages_matrix_name}.zip"
}

# Creating a GCS object in Functions bucket to deploy the function in GCP
resource "google_storage_bucket_object" "gcp_function_messages_matrix_code" {
  name   = "code/${var.cfunction_messages_matrix_name}/${data.archive_file.gcp_function_messages_matrix_code.output_sha}.zip"
  bucket = var.gcp_bucket_functions_name
  source = data.archive_file.gcp_function_messages_matrix_code.output_path
}

# Creating a control file for messages-to-matrix function
data "local_file" "messages_to_matrix" {
  filename = "${path.module}/code/messages-to-matrix/tracking.file"
}

# Code to deploy the messages-to-matrix function in GCP
resource "google_cloudfunctions_function" "gcp_function_messages_matrix" {
  name        = var.cfunction_messages_matrix_name
  description = "Function to connect to send messages to a Matrix room"
  runtime     = "python310"
  entry_point = var.cfunction_messages_matrix_name

  region = var.gcp_default_region

  source_archive_bucket = var.gcp_bucket_functions_name
  source_archive_object = google_storage_bucket_object.gcp_function_messages_matrix_code.name

  available_memory_mb = 256

  max_instances = 1

  event_trigger {
    event_type = "google.pubsub.topic.publish"
    resource   = "projects/${var.gcp_default_project}/topics/${var.matrix_notifications_topic_name}"
    failure_policy {
      retry = false
    }
  }

  labels = merge(
    local.labels,
    { "app" : "alerting" },
    jsondecode(data.local_file.messages_to_matrix.content)
  )

  environment_variables = {
    "GCS_BUCKET" = var.gcp_bucket_functions_name
  }

  service_account_email = google_service_account.gcp_functions_sa.email

  project = var.gcp_default_project

  depends_on = [google_storage_bucket_object.gcp_function_messages_matrix_code]
}


# Resources related to gcp-billing-notifications Cloud Function
# This function will connect to the BigQuery billing dataset and
# will retrieve the total daily cost and if its above one threshold
# it will create a message in PubSub.

# Zipping gcp-billing-notifications function to upload it to GCS
data "archive_file" "gcp_function_checking_billing_code" {
  type        = "zip"
  source_dir  = "${path.module}/code/gcp-billing-notifications"
  output_path = "${path.module}/code/dest/${var.cfunction_checking_billing_name}.zip"
}

# Creating a GCS object in Functions bucket to deploy the function in GCP
resource "google_storage_bucket_object" "gcp_function_checking_billing_code" {
  name   = "code/${var.cfunction_checking_billing_name}/${data.archive_file.gcp_function_checking_billing_code.output_sha}.zip"
  bucket = var.gcp_bucket_functions_name
  source = data.archive_file.gcp_function_checking_billing_code.output_path
}

# Creating a control file for gcp-billing-notifications function
data "local_file" "gcp_billing_notifications" {
  filename = "${path.module}/code/gcp-billing-notifications/tracking.file"
}

# Code to deploy the gcp-billing-notifications function in GCP
resource "google_cloudfunctions_function" "gcp_function_checking_billing" {
  name        = var.cfunction_checking_billing_name
  description = "Function to connect to check the status of your billing"
  runtime     = "python310"
  entry_point = var.cfunction_checking_billing_name

  region = var.gcp_default_region

  source_archive_bucket = var.gcp_bucket_functions_name
  source_archive_object = google_storage_bucket_object.gcp_function_checking_billing_code.name

  available_memory_mb = 256

  max_instances = 1

  event_trigger {
    event_type = "google.pubsub.topic.publish"
    resource   = "projects/${var.gcp_default_project}/topics/${var.orchestator_topic_name}"
    failure_policy {
      retry = false
    }
  }

  labels = merge(
    local.labels,
    { "app" : "alerting" },
    jsondecode(data.local_file.gcp_billing_notifications.content)
  )

  environment_variables = {
    "THRESHOLD"        = 1
    "INTERVAL"         = 2
    "BILLING_PROJECT"  = var.env == "prd" ? var.gcp_default_project : substr(var.gcp_default_project, 0, 12)
    "BILLING_DATASET"  = var.billing_dataset_name
    "BILLING_TABLE"    = "gcp_billing_export_v1_${replace(data.google_billing_account.tangelov.id, "-", "_")}"
    "GCP_PUBSUB_TOPIC" = "projects/${var.gcp_default_project}/topics/${var.matrix_notifications_topic_name}"
  }

  service_account_email = google_service_account.gcp_functions_sa.email

  project = var.gcp_default_project

  depends_on = [google_storage_bucket_object.gcp_function_checking_billing_code]
}
