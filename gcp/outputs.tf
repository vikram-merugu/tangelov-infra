# Outputs file
output "sops_account_key" {
  value       = var.env == "prd" ? google_service_account_key.sops[0].private_key : null
  description = "Service Account key to decrypt SOPS secrets"
  sensitive = true
}

output "external_account_key" {
  value       = google_service_account_key.external.private_key
  description = "Service Account key to send messages to PubSub topics"
  sensitive = true
}
