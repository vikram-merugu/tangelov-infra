# Resources related to Cloud PubSub and Cloud Scheduler
resource "google_project_service" "cloud_scheduler_api" {
  project = data.google_project.tangelov_project.project_id
  service = "cloudscheduler.googleapis.com"

  disable_dependent_services = true
}

resource "google_cloud_scheduler_job" "cscheduler_backups" {
  name        = var.cscheduler_backup_checker_name
  description = var.cscheduler_backup_checker_description
  schedule    = var.cscheduler_backup_checker_cron
  time_zone   = var.cscheduler_backup_checker_timezone

  region = var.gcp_app_engine_location == "europe-west" ? "europe-west1" : var.gcp_app_engine_location # Fix inconsistency names

  pubsub_target {
    topic_name = google_pubsub_topic.orchestator.id
    data       = base64encode(var.cscheduler_backup_checker_payload)
  }
}

# Topic to orchestrate all the process via PubSub
resource "google_pubsub_topic" "orchestator" {
  name = var.orchestator_topic_name

  project = var.gcp_default_project

  labels = merge(
    local.labels,
    { "app" : "alerting" }
  )
}

# Topic in Google Cloud PubSub to send notifications
resource "google_pubsub_topic" "matrix_notifications" {
  name = var.matrix_notifications_topic_name

  project = var.gcp_default_project

  labels = merge(
    local.labels,
    { "app" : "alerting" }
  )
}

# Permissions to put messages in our PubSub topic
resource "google_pubsub_topic_iam_member" "matrix_not_publisher_iam" {
  topic  = google_pubsub_topic.matrix_notifications.name
  role   = "roles/pubsub.publisher"
  member = "serviceAccount:${google_service_account.gcp_functions_sa.email}"

  project = var.gcp_default_project
}

resource "google_pubsub_topic_iam_member" "matrix_not_external_publisher_iam" {
  topic  = google_pubsub_topic.matrix_notifications.name
  role   = "roles/pubsub.publisher"
  member = "serviceAccount:${google_service_account.external.email}"

  project = var.gcp_default_project
}
