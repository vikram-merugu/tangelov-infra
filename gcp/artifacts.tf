resource "google_project_service" "artifacts_api" {
  project = data.google_project.tangelov_project.project_id
  service = "artifactregistry.googleapis.com"

  disable_dependent_services = true
}
