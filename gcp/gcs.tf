# Resources related to Google Cloud Storage
#tfsec:ignore:google-storage-bucket-encryption-customer-key
resource "google_storage_bucket" "gcp_bucket_images" {
  count    = var.env == "prd" ? 1 : 0
  name     = var.gcp_bucket_images_name
  location = var.gcp_default_region

  uniform_bucket_level_access = true

  storage_class = var.gcp_bucket_images_storage_class
  project       = var.gcp_default_project

  versioning {
    enabled = true
  }

  labels = merge(
    local.labels,
    { "app" : "blog" }
  )

  lifecycle {
    prevent_destroy = true
  }
}

# IAM binding to make the images bucket public to store statics
#tfsec:ignore:google-storage-no-public-access
resource "google_storage_bucket_iam_member" "gcp_bucket_images_public" {
  count  = var.env == "prd" ? 1 : 0
  bucket = google_storage_bucket.gcp_bucket_images[count.index].name
  role   = "roles/storage.objectViewer"
  member = "allUsers"
}

# Private bucket to store resources related to Cloud Functions deployment
#tfsec:ignore:google-storage-bucket-encryption-customer-key
resource "google_storage_bucket" "gcp_bucket_functions" {
  name     = var.gcp_bucket_functions_name
  location = var.gcp_default_region

  uniform_bucket_level_access = true

  storage_class = var.gcp_bucket_images_storage_class
  project       = var.gcp_default_project

  versioning {
    enabled = true
  }

  labels = merge(
    local.labels,
    { "app" : "alerting" }
  )

  lifecycle {
    prevent_destroy = true
  }
}

# Permissions extra to add to Cloud Functions account
resource "google_storage_bucket_iam_member" "functions_publisher_legacy_bucket_iam" {
  bucket = google_storage_bucket.gcp_bucket_functions.name
  role   = "roles/storage.legacyBucketReader"
  member = "serviceAccount:${google_service_account.gcp_functions_sa.email}"
}

resource "google_storage_bucket_iam_member" "functions_publisher_admin_bucket_iam" {
  bucket = google_storage_bucket.gcp_bucket_functions.name
  role   = "roles/storage.objectAdmin"
  member = "serviceAccount:${google_service_account.gcp_functions_sa.email}"
}

# Adding some extra permissions in Prod to be able to send messages
resource "google_storage_bucket_iam_member" "dev_functions_publisher_legacy_bucket_iam" {
  count  = var.env == "prd" ? 1 : 0
  bucket = google_storage_bucket.gcp_bucket_functions.name
  role   = "roles/storage.legacyBucketReader"
  member = "serviceAccount:${var.sa_functions_account_id}@${data.google_project.tangelov_project.name}${var.alt_environment}.iam.gserviceaccount.com"
}

resource "google_storage_bucket_iam_member" "dev_functions_publisher_admin_bucket_iam" {
  count  = var.env == "prd" ? 1 : 0
  bucket = google_storage_bucket.gcp_bucket_functions.name
  role   = "roles/storage.objectAdmin"
  member = "serviceAccount:${var.sa_functions_account_id}@${data.google_project.tangelov_project.name}${var.alt_environment}.iam.gserviceaccount.com"
}
