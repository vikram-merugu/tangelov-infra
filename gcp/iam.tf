# Enabling the Service Usage API to work in the project
resource "google_project_service" "service_usage_api" {
  project = data.google_project.tangelov_project.project_id
  service = "serviceusage.googleapis.com"

  disable_dependent_services = true
}

# Service account to deploy Google App Engine applications
resource "google_service_account" "gae_deployer_sa" {
  account_id   = var.sa_deployer_account_id
  display_name = var.sa_deployer_display_name
  description  = var.sa_deployer_description

  project = var.gcp_default_project
}

# Service account to access to Cloud Pub Sub
resource "google_service_account" "gcp_functions_sa" {
  account_id   = var.sa_functions_account_id
  display_name = var.sa_functions_display_name
  description  = var.sa_functions_description

  project = var.gcp_default_project
}

# Service account to access to SOPS
resource "google_service_account" "sops" {
  count        = var.env == "prd" ? 1 : 0
  account_id   = var.sa_sops_account_id
  display_name = var.sa_sops_display_name
  description  = "Account to encrypt and decrypt secrets in repositories with SOPS."

  project = var.gcp_default_project
}

resource "google_service_account_key" "sops" {
  count              = var.env == "prd" ? 1 : 0
  service_account_id = google_service_account.sops[count.index].name
  public_key_type    = "TYPE_X509_PEM_FILE"
}

# Service account to access to PubSub from outside of GCP
resource "google_service_account" "external" {
  account_id   = var.sa_external_account_id
  display_name = var.sa_external_display_name
  description  = "Account to send messages to PubSub topics."

  project = var.gcp_default_project
}

resource "google_service_account_key" "external" {
  service_account_id = google_service_account.external.name
  public_key_type    = "TYPE_X509_PEM_FILE"
}
