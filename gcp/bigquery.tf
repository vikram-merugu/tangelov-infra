# Resources related to BigQuery assets. All this resources are only created in Production
resource "google_bigquery_dataset" "logging" {
  count         = var.env == "prd" ? 1 : 0
  dataset_id    = var.gcp_bq_dataset_exports_id
  friendly_name = var.gcp_bq_dataset_exports_name
  description   = var.gcp_bq_dataset_exports_description
  location      = var.gcp_bq_dataset_exports_location

  labels = merge(
    local.labels,
    { "app" : "monitoring" }
  )
}

# Resources related to a Logging BigQuery Sink.
# We use the request logged by Cloud Monitoring (formerly known as Stackdriver)
# to have some insights about who is visiting the blog.
resource "google_logging_project_sink" "stackdriver_bq_requests_sink" {
  count       = var.env == "prd" ? 1 : 0
  name        = var.gcp_project_sink_gae_requests_name
  destination = "bigquery.googleapis.com/projects/${var.gcp_default_project}/datasets/${var.gcp_bq_dataset_exports_name}"
  filter      = "resource.type=\"gae_app\"\nresource.labels.module_id=\"default\"\nlogName=\"projects/${var.gcp_default_project}/logs/appengine.googleapis.com%2Frequest_log\""

  unique_writer_identity = true
}

# Some permissions are required to give the Exporter the needed permissions to put the logs from Cloud Monitoring into BigQuery
resource "google_project_iam_binding" "bq_request_exporter" {
  count = var.env == "prd" ? 1 : 0
  role  = "roles/bigquery.dataEditor"

  members = [
    google_logging_project_sink.stackdriver_bq_requests_sink[count.index].writer_identity,
  ]

  project = data.google_project.tangelov_project.id
}

# BigQuery resources needed for Looker Studio
# Most of the public datasets maintained by Google are located in the US and we need to 
# use our data with them. You can't do multi-region queries in BQ therefore we copy the data
# from Europe to US.
resource "google_bigquery_dataset" "logging_us" {
  count         = var.env == "prd" ? 1 : 0
  dataset_id    = "${var.gcp_bq_dataset_exports_id}_us"
  friendly_name = "${var.gcp_bq_dataset_exports_name}_us"
  location      = var.gcp_bq_dataset_exports_alt_location

  labels = merge(
    local.labels,
    { "app" : "monitoring" }
  )

  depends_on = [google_project_iam_member.data_transfer_user]
}

# IAM Permissions needed to transfer the data from Europe to US
resource "google_project_iam_member" "data_transfer_user" {
  count  = var.env == "prd" ? 1 : 0
  role   = "roles/iam.serviceAccountShortTermTokenMinter"
  member = "serviceAccount:service-${data.google_project.tangelov_project.number}@gcp-sa-bigquerydatatransfer.iam.gserviceaccount.com"

  project = data.google_project.tangelov_project.id

  depends_on = [google_project_service.bq_data_transfer_api]
}

# Enabling BigQuery Data Transfer API
resource "google_project_service" "bq_data_transfer_api" {
  count   = var.env == "prd" ? 1 : 0
  project = data.google_project.tangelov_project.project_id
  service = "bigquerydatatransfer.googleapis.com"

  disable_dependent_services = true
}

# Transferencia de datos entre EU y US para cruzar datos con datasets publicos
resource "google_bigquery_data_transfer_config" "transfer_from_eu_to_us" {
  count                  = var.env == "prd" ? 1 : 0
  display_name           = var.gcp_bq_dataset_exports_data_transfer_dname
  data_source_id         = "cross_region_copy"
  schedule               = "every day 16:30"
  destination_dataset_id = google_bigquery_dataset.logging_us[count.index].dataset_id

  disabled                 = false
  data_refresh_window_days = 0

  params = {
    overwrite_destination_table = "true"
    source_dataset_id           = google_bigquery_dataset.logging[count.index].dataset_id
    source_project_id           = data.google_project.tangelov_project.project_id
  }

  schedule_options {
    disable_auto_scheduling = false
    start_time              = "2021-01-17T16:30:00Z"
  }

  depends_on = [google_project_iam_member.data_transfer_user]
}

# Permissions to create read BigQuery data to the Cloud Functions account
resource "google_bigquery_table_iam_member" "billing_reader" {
  count      = var.env == "prd" ? 1 : 0
  project    = data.google_project.tangelov_project.project_id
  dataset_id = var.billing_dataset_name
  table_id   = "gcp_billing_export_v1_${replace(data.google_billing_account.tangelov.id, "-", "_")}"
  role       = "roles/bigquery.dataViewer"
  member     = "serviceAccount:${google_service_account.gcp_functions_sa.email}"
}

# Permissions to create BigQuery jobs to the Cloud Functions account
resource "google_project_iam_member" "billing_job_creator" {
  count   = var.env == "prd" ? 1 : 0
  project = data.google_project.tangelov_project.project_id
  role    = "roles/bigquery.jobUser"
  member  = "serviceAccount:${google_service_account.gcp_functions_sa.email}"
}

# Permissions to be able to create read sessions in BigQuery to the Cloud Functions account
resource "google_project_iam_member" "billing_readsession_user" {
  count   = var.env == "prd" ? 1 : 0
  project = data.google_project.tangelov_project.project_id
  role    = "roles/bigquery.readSessionUser"
  member  = "serviceAccount:${google_service_account.gcp_functions_sa.email}"
}


# Permissions related with reading data from Billing table for Dev account
resource "google_bigquery_table_iam_member" "dev_billing_reader" {
  count      = var.env == "prd" ? 1 : 0
  project    = data.google_project.tangelov_project.project_id
  dataset_id = var.billing_dataset_name
  table_id   = "gcp_billing_export_v1_${replace(data.google_billing_account.tangelov.id, "-", "_")}"
  role       = "roles/bigquery.dataViewer"
  member     = "serviceAccount:${var.sa_functions_account_id}@${data.google_project.tangelov_project.name}${var.alt_environment}.iam.gserviceaccount.com"
}

# Permissions related to creating BigQuery jobs from Billing table for Dev account
resource "google_project_iam_member" "dev_billing_job_creator" {
  count   = var.env == "prd" ? 1 : 0
  project = data.google_project.tangelov_project.project_id
  role    = "roles/bigquery.jobUser"
  member  = "serviceAccount:${var.sa_functions_account_id}@${data.google_project.tangelov_project.name}${var.alt_environment}.iam.gserviceaccount.com"
}

# Permissions to be able to create read sessions in BigQuery for Dev account
resource "google_project_iam_member" "dev_billing_readsession_user" {
  count   = var.env == "prd" ? 1 : 0
  project = data.google_project.tangelov_project.project_id
  role    = "roles/bigquery.readSessionUser"
  member  = "serviceAccount:${var.sa_functions_account_id}@${data.google_project.tangelov_project.name}${var.alt_environment}.iam.gserviceaccount.com"
}
