#!/bin/bash

# Obteniendo información de las versiones de las funciones a desplegar
# Primero definimos todas las variables que contienen la configuración de las funciones
#FUNCTION_VARS="CFUNCTION_DRIVE_BACKUPS CFUNCTION_MESSAGES_MATRIX CFUNCTION_GCP_BILLING"
FUNCTION_VARS="CFUNCTION_MESSAGES_MATRIX CFUNCTION_GCP_BILLING"

# Limpiamos de forma preventiva el directorio de las funciones
rm -rf code/*

# Recorremos las variables que mantienen la sincronización entre funciones y Terraform
for VAR in $FUNCTION_VARS
do
  # Si estamos ejecutando fuera de Gitlab CI, obtenemos la información de la variable sin limpiar
  # de la API de Gitlab
  if [[ "${GITLAB_CI}" != "true" ]]; then
    RAW=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
    "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/variables/${VAR}" | jq '.value')

    # Parseamos la información de la respuesta de la API de Gitlab
    CLEAN=$(echo ${RAW:1:-1})

  else
    # Obtenemos el valor directamente de la variable de entorno
    RAW=$(printenv $VAR)
    # Si estamos dentro de Gitlab CI no es necesario parsear nada
    CLEAN=$RAW
  fi
  
  # Limpiamos la información de cada valor
  FUNCTION_NAME=$(printf $CLEAN | jq '.name'); FUNCTION_NAME=$(echo ${FUNCTION_NAME:1:-1})
  FUNCTION_URL=$(printf $CLEAN | jq '.url'); FUNCTION_URL=$(echo ${FUNCTION_URL:1:-1})
  FUNCTION_TAG=$(printf $CLEAN | jq '.tag'); FUNCTION_TAG=$(echo ${FUNCTION_TAG:1:-1})

  # Clonamos los repositorios donde toca y creamos un fichero temporal de control
  git clone --branch ${FUNCTION_TAG} ${FUNCTION_URL}.git code/${FUNCTION_NAME}
  printf $CLEAN > code/$FUNCTION_NAME/tracking.file

  # Tranformamos FUNCTION_URL para evitar los caracteres prohibidos por GCP en las etiquetas. Limpiamos FUNCTION_URL también
  FUNCTION_URL=$(echo ${FUNCTION_URL::-1})
  CLEAN_URL=$(echo ${FUNCTION_URL:8}); CLEAN_URL=$(echo $CLEAN_URL | sed -e 's/\//_/g' -e 's/\./_/g')

  # Tranformamos FUNCTION_TAG para evitar los caracteres prohibidos por GCP en las etiquetas. Limpiamos FUNCTION_TAG también
  CLEAN_TAG=$(echo $FUNCTION_TAG | sed -e 's/\./_/g')

  # Reemplazamos la URL y el TAG en el fichero generado
  sed -i "s#$FUNCTION_URL#$CLEAN_URL#g" code/$FUNCTION_NAME/tracking.file
  sed -i "s#$FUNCTION_TAG#$CLEAN_TAG#g" code/$FUNCTION_NAME/tracking.file
done
