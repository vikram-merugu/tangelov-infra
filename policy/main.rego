# Este fichero contiene las políticas generales que se definen para todos los recursos de Terraform
package main

import data.functions

## Evitamos que un recurso pueda ser eliminado automáticamente por el pipeline
deny[msg] {
    # Asignando los cambios a evitar en Terraform
    avoid := "delete"

    # Asignamos cada recurso y sus cambios a la variable item
    item = input.resource_changes[_]

    # Iteramos sobre una los distintos cambios aplicados en cada recurso y verificamos si alguno es delete
    # Nos saltamos cualquier tipo de objeto que sea un objeto dentro de un bucket puesto que actualmente
    # todos los datos que tengo almacenados en ellos salvo las imágenes (que no están terraformadas), son
    # efimeros y tiene sentido que se vayan recreando periódicamente.
    some i
        item.type != "google_storage_bucket_object"
        item.change.actions[i] == avoid

        msg = sprintf(
            "El recurso '%v' de tipo '%v' va a ser destruido o reemplazado. Abortando",
            [item.name, item.type]
        )
}

## Lanzamos un error si alguna de las variables definidas en Terraform no tiene ninguna etiqueta. Pasará por
## todas las variables del terraform plan.
deny[msg] {
    # Asociamos a item cada variable
	item = input.configuration.root_module.variables[key]

    # Comprobamos que cada item tiene una descripcion asociada
	functions.get_desc(item) == ""

	msg := sprintf("La variable '%v' no tiene una descripción asociada", 
	    [key]
    )
}

## Lanzamos un error si alguno de los recursos que utilizamos esta mal etiquetado. Pasará por todos los
## recursos que hayamos definido y que tenga disponible el uso de tags o labels (dependiendo del proveedor)
## Está basado en https://github.com/Scalr/sample-tf-opa-policies/blob/master/management/resource_tags.rego
deny[msg] {
    # Asignamos los cambios a la variable item
    item := input.resource_changes[_]
    # Comprobamos que se está aplicando algún cambio al recurso y si existe lo asignamos a la variable
    # action
    action := item.change.actions[count(item.change.actions) - 1]
    # Vemos si alguno de los cambios aplicados son create o update con la función array_contains
    functions.array_contains(["create", "update"], action)

    # Utilizamos la funcion get_tags para ver que labels/tags tiene cada recurso
    tags := functions.get_tags(item)
    # Generamos un array con las tags que contiene el recurso y otro con los que consideramos requisito
    existing_tags := [ key | tags[key] ]
    required_tag := functions.required_tags[_]
    # Si el objeto no tiene o va a perder dichos tags, devolvemos un error
    not functions.array_contains(existing_tags, required_tag)

    msg := sprintf(
        "El recurso '%v de tipo '%v' no tiene asignada la siguiente tag/label %q. Abortando",
        [item.name, item.address, required_tag]
    )
}

## Lanzamos un error si alguno de los buckets que estamos utilizado no tiene habilitado el sistema de
## versionado. De esta forma tenemos backups de los objetos que utilizamos y pueden ser recuperados de 
## manera rápida y sencilla. Más políticas pueden obtenerse de aquí: 
## https://github.com/forseti-security/terraform-google-forseti/tree/master/modules/real_time_enforcer/files/policy
deny[msg] {
    # Asignamos los cambios a la variable item
    item := input.resource_changes[_]

    # Comprobamos que el item es del tipo google_storage_bucket
    item.type == "google_storage_bucket"

    # Comprobamos que se está aplicando algún cambio al recurso y si existe lo asignamos a la variable
    # action
    action := item.change.actions[count(item.change.actions) - 1]
    # Vemos si alguno de los cambios aplicados son create o update con la función array_contains
    functions.array_contains(["create", "update"], action)

    # Comprobamos que tiene el versionado con el valor true
    versioning = functions.get_versioning(item)
    versioning != true

    msg := sprintf(
        "El recurso '%v' de tipo '%v' no tiene habilitado el versionado. Abortando",
        [item.address, item.type]
    )
}

## Verificamos que nuestra acl para convertir los objetos de un bucket en públicos y asegurar
## que las imágenes de nuestro blog son accesibles de forma pública.
deny[msg] {
    # Asignamos los cambios a la variable item, el bucket a comprobar y los permisos a verificar
    item := input.resource_changes[_]
    bucket := "tangelov-data"
    public_access := "READER:allUsers"

    # Comprobamos que el item es del tipo google_storage_bucket_default_object_acl
    item.type == "google_storage_default_object_acl"

    # Comprobamos que se está aplicando algún cambio al recurso y si existe lo asignamos a la variable
    # action
    action := item.change.actions[count(item.change.actions) - 1]
    # Vemos si alguno de los cambios aplicados son create o update con la función array_contains
    functions.array_contains(["create", "update"], action)

    # Comprobamos que tiene está asociada al único bucket que tiene que ser público
    item.change.after.bucket == bucket

    # Comprobamos los diferentes permisos que posee la ACL 
    bucket_permissions := item.change.after.role_entity
    not functions.array_contains(bucket_permissions, public_access)
    
    msg := sprintf(
        "La ACL '%v' no está asignada al bucket '%v' y debería ser público. Abortando",
        [item.address, bucket]
    )
}