## Rego es un lenguaje que necesitamos extender para cumplir algunos casos de uso.
## En este fichero van a poner las funciones comunes a todos los proveedores y que 
## vamos a utilizar para extender la funcionalidad de los tests que escribamos.
package functions

## Variables para testing
## Definimos las variables obligatorias en nuestros recursos
required_tags = ["environment", "management-tool", "app"]

## Funcion array_contains comprueba si un elemento está dentro de un array
## La funcion devuelve true, si un elemento está en el array 
## y devuelve false si se cumple la condición dentro de los corchetes 
## (que al ser true, es siempre que no se cumpla la primera)
array_contains(arr, elem) = true {
  arr[_] = elem
} 
else = false { 
  true 
}

## Función get_basename comprueba si un elemento es de un determinado proveedor o no
get_basename(path) = basename{
    arr := split(path, "/")
    basename:= arr[count(arr)-1]
}

## Función get_tags obtiene todos los tags/labels en función del proveedor. En Google
## son llamados labels y por eso se realiza un filtrado previo por proveedor. Si no tiene
## tags, devuelve una lista vacía como podemos ver en sus condicionales.
get_tags(resource) = labels {
    provider_name := get_basename(resource.provider_name)
    "google" == provider_name
    labels := resource.change.after.labels
} else = tags {
    tags := resource.change.after.tags
}

## Función get_desc te permite verificar si las variables utilizadas en terraform tienen
## alguna descripcion asociada. Si la variable tiene alguna descripcion asociada le asignamos
## dicho valor y en caso contrario devolvemos ""
get_desc(var) = desc {
  desc := var.description
} else = no_desc {
  no_desc := ""
}

## Función para verificar si un elemento de tipo bucket en GCP tiene habilitado el versioning
## Comprueba si existe la key enabled dentro de la configuración de versioning y en caso de que
# no exista o tenga cualquier otro valor de true, devuelve un false.
get_versioning(resource) = versioning {
    versioning := resource.change.after.versioning[_].enabled
    versioning == true
} else = no_versioning {
    no_versioning := false
}